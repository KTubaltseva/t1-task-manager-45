package ru.t1.ktubaltseva.tm.dto.response.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationAboutResponse extends AbstractResponse {

    private String author;

    private String email;

    private String branch;

    private String gitCommitId;

    private String gitCommitter;

    private String gitCommitterEMail;

    private String gitCommitMessage;

    private String gitCommitTime;

}
