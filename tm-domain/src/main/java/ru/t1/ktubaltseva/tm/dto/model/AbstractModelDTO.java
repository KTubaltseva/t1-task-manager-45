package ru.t1.ktubaltseva.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public class AbstractModelDTO implements Serializable {

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "varchar(36)", nullable = false)
    protected String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "created", columnDefinition = "timestamp default current_date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    protected Date created = new Date();

}
