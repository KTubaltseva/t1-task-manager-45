package ru.t1.ktubaltseva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserUnlockRequest(@Nullable final String token) {
        super(token);
    }

    public UserUnlockRequest(@Nullable final String token, @Nullable final String login) {
        super(token);
        this.login = login;
    }

}
