package ru.t1.ktubaltseva.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectCreateRequest(@Nullable final String token) {
        super(token);
    }

    public ProjectCreateRequest(@Nullable final String token, @Nullable final String name, @Nullable final String description) {
        super(token);
        this.name = name;
        this.description = description;
    }

}
