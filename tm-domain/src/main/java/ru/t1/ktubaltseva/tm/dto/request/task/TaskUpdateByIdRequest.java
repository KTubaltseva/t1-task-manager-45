package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest(@Nullable final String token) {
        super(token);
    }

    public TaskUpdateByIdRequest(@Nullable final String token, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        super(token);
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
