package ru.t1.ktubaltseva.tm.exception.auth;

public class AuthRequiredException extends AbstractAuthException {

    public AuthRequiredException() {
        super("Error! Authentication required...");
    }

}