package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.user.UserDisplayProfileRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktubaltseva.tm.dto.response.user.UserDisplayProfileResponse;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface IAuthEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserDisplayProfileResponse getProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserDisplayProfileRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) throws AbstractException;

}
