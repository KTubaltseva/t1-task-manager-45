package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelWBSDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(name = "name", columnDefinition = "varchar(30)", nullable = false)
    protected String name = "";

    @Nullable
    @Column(name = "description", columnDefinition = "varchar(255)")
    protected String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "varchar(30)", nullable = false)
    protected Status status = Status.NOT_STARTED;

    public AbstractUserOwnedModelWBSDTO(@NotNull final String name) {
        this.name = name;
    }

    public AbstractUserOwnedModelWBSDTO(
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public AbstractUserOwnedModelWBSDTO(
            @NotNull final UserDTO user,
            @NotNull final String name
    ) {
        this.userId = user.getId();
        this.name = name;
    }

    public AbstractUserOwnedModelWBSDTO(
            @NotNull final UserDTO user,
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.userId = user.getId();
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
