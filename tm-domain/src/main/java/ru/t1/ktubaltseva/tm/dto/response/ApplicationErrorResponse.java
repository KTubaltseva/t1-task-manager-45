package ru.t1.ktubaltseva.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
