package ru.t1.ktubaltseva.tm.dto.request.system;

import lombok.NoArgsConstructor;
import ru.t1.ktubaltseva.tm.dto.request.AbstractRequest;

@NoArgsConstructor
public class ApplicationVersionRequest extends AbstractRequest {
}
