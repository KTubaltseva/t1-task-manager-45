package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class ProjectTaskTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static String USER_2_LOGIN = "USER_2_LOGIN";

    @Nullable
    public final static String USER_2_PASSWORD = "USER_2_PASSWORD";

    @Nullable
    public final static String USER_2_EMAIL = "USER_2_EMAIL";

    @NotNull
    public final static UserDTO USER_1 = new UserDTO(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);

    @NotNull
    public final static UserDTO USER_2 = new UserDTO(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);

    @Nullable
    public final static TaskDTO NULL_TASK = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static String NULL_TASK_ID = null;

    @Nullable
    public final static String NON_EXISTENT_TASK_ID = "NON_EXISTENT_TASK_ID";

    @Nullable
    public final static String NULL_PROJECT_ID = null;

    @Nullable
    public final static String NON_EXISTENT_PROJECT_ID = "NON_EXISTENT_PROJECT_ID";

    @Nullable
    public final static TaskDTO NON_EXISTENT_TASK = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_1_TASK_1 = new TaskDTO(USER_1, "name1");

    @NotNull
    public final static TaskDTO USER_1_TASK_2 = new TaskDTO(USER_1, "name2");

    @NotNull
    public final static TaskDTO USER_2_TASK_1 = new TaskDTO(USER_2, "name3");

    @NotNull
    public final static TaskDTO USER_2_TASK_2 = new TaskDTO(USER_2, "name4");

    @NotNull
    public final static ProjectDTO USER_1_PROJECT_1 = new ProjectDTO(USER_1, "name1");

    @NotNull
    public final static ProjectDTO USER_1_PROJECT_2 = new ProjectDTO(USER_1, "name2");

    @NotNull
    public final static List<TaskDTO> USER_1_TASK_LIST = Arrays.asList(USER_1_TASK_1, USER_1_TASK_2);

    @NotNull
    public final static List<TaskDTO> USER_2_TASK_LIST = Arrays.asList(USER_2_TASK_1, USER_2_TASK_2);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = Arrays.asList(USER_1_TASK_1, USER_1_TASK_2, USER_2_TASK_1, USER_2_TASK_2);

    @Nullable
    public final static String NULL_NAME = null;

    @Nullable
    public final static String NULL_DESC = null;

    @Nullable
    public final static String TASK_NAME = "TASK_NAME";

    @Nullable
    public final static String TASK_DESC = "TASK_DESC";

    @Nullable
    public final static Status NULL_STATUS = null;

    @Nullable
    public final static Status TASK_STATUS = Status.COMPLETED;

}
