package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final IProjectDTORepository repository = new ProjectDTORepository(entityManager);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.remove(USER_1);
        userService.remove(USER_2);
        entityManager.close();
    }

    @Before
    @SneakyThrows
    public void before() {
        if (entityManager.getTransaction().isActive())
            entityManager.getTransaction().rollback();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final ProjectDTO project : PROJECT_LIST) {
            try {
                entityManager.getTransaction().begin();
                repository.removeById(project.getId());
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
            }

        }
        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        }

        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_2.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final ProjectDTO projectToAdd = USER_1_PROJECT_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        entityManager.getTransaction().begin();
        repository.add((projectToAdd));
        entityManager.getTransaction().commit();

        @Nullable final ProjectDTO projectFindOneById = repository.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;

        entityManager.getTransaction().begin();
        repository.add(projectExists);
        entityManager.getTransaction().commit();

        @Nullable final ProjectDTO projectFindOneById = repository.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());

        @Nullable final ProjectDTO projectFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_PROJECT_ID);
        Assert.assertNull(projectFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;

        entityManager.getTransaction().begin();
        repository.add(projectExists);
        entityManager.getTransaction().commit();

        Assert.assertNull(repository.findOneById(projectExists.getUserId(), NON_EXISTENT_PROJECT_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, projectExists.getId()));

        @Nullable final ProjectDTO projectFindOneById = repository.findOneById(projectExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;

        entityManager.getTransaction().begin();
        repository.add(projectExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<ProjectDTO> projectsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final ProjectDTO projectExists = USER_1_PROJECT_1;
        @NotNull final UserDTO userExists = USER_1;

        entityManager.getTransaction().begin();
        repository.add(projectExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<ProjectDTO> projectsFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepNoEmpty);
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final ProjectDTO projectByUserToClear = USER_1_PROJECT_1;
        @NotNull final ProjectDTO projectByUserNoClear = USER_2_PROJECT_2;

        entityManager.getTransaction().begin();
        repository.add(projectByUserToClear);
        repository.add(projectByUserNoClear);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.clear(userToClearId);
        entityManager.getTransaction().commit();

        @Nullable final ProjectDTO projectFindOneByIdToClear = repository.findOneById(userToClearId, projectByUserToClear.getId());
        Assert.assertEquals(0, repository.getSize(userToClearId));
        Assert.assertNull(projectFindOneByIdToClear);

        @Nullable final ProjectDTO projectFindOneByIdNoClear = repository.findOneById(projectByUserNoClear.getId());
        Assert.assertEquals(projectByUserNoClear.getId(), projectFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.getSize(userNoClearId));
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final ProjectDTO projectToRemove = USER_1_PROJECT_1;

        entityManager.getTransaction().begin();
        repository.add((projectToRemove));
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.removeById(projectToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final ProjectDTO projectFindOneById = repository.findOneById(projectToRemove.getId());
        Assert.assertNull(projectFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final ProjectDTO projectByUserToRemove = USER_1_PROJECT_1;
        @Nullable final ProjectDTO projectByUserNoRemove = USER_2_PROJECT_1;

        entityManager.getTransaction().begin();
        repository.add((projectByUserToRemove));
        repository.add((projectByUserNoRemove));
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.removeById(userToRemoveId, projectByUserToRemove.getId());
        entityManager.getTransaction().commit();
        @Nullable final ProjectDTO projectRemovedFindOneById = repository.findOneById(projectByUserToRemove.getId());
        Assert.assertNull(projectRemovedFindOneById);

        entityManager.getTransaction().begin();
        repository.removeById(userToRemoveId, projectByUserNoRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final ProjectDTO projectNoRemovedFindOneById = repository.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

}
