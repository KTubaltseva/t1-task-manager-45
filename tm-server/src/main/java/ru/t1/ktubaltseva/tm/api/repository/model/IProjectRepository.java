package ru.t1.ktubaltseva.tm.api.repository.model;

import ru.t1.ktubaltseva.tm.model.Project;

public interface IProjectRepository extends IUserOwnedWBSRepository<Project> {

}
