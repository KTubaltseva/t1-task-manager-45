package ru.t1.ktubaltseva.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextInt() throws NumberIncorrectException {
        @NotNull final String string = nextLine();
        try {
            return Integer.parseInt(string);
        } catch (@NotNull final RuntimeException e) {
            throw new NumberIncorrectException(string);
        }
    }

}
