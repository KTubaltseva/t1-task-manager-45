package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.util.FormatUtil;

public final class ApplicationInfoCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "info";

    @NotNull
    private final String DESC = "Display system info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-i";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() {
        @NotNull final Runtime runtime = Runtime.getRuntime();

        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final long totalMemory = runtime.totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        @NotNull final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.formatBytes(maxMemory));
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        System.out.printf("Available processors (cores):\t %s\n", availableProcessors);
        System.out.println();
        System.out.printf("Free memory:\t %s\n", freeMemoryFormat);
        System.out.printf("Maximum memory:\t %s\n", maxMemoryFormat);
        System.out.printf("Total memory:\t %s\n", totalMemoryFormat);
        System.out.printf("Usage memory:\t %s\n", usageMemoryFormat);
        System.out.println();
    }

}
