package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.ktubaltseva.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String DESC = "Display developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-a";
        return ARGUMENT;
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getAbout(request);
        System.out.println("[ABOUT]");
        System.out.println("Author: " + response.getAuthor());
        System.out.println("E-mail: " + response.getEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("Branch: " + response.getBranch());
        System.out.println("Commit Id: " + response.getGitCommitId());
        System.out.println("Committer: " + response.getGitCommitter());
        System.out.println("E-mail: " + response.getGitCommitterEMail());
        System.out.println("Message: " + response.getGitCommitMessage());
        System.out.println("Time: " + response.getGitCommitTime());
    }

}
