package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-complete-by-id";

    @NotNull
    private final String DESC = "Complete task by Id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken(), id);
        getTaskEndpoint().completeTaskById(request);
    }

}
