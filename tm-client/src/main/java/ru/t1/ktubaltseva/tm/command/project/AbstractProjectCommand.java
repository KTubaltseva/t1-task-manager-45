package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void displayProject(@Nullable final ProjectDTO project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESC: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("USER ID: " + project.getUserId());
    }

    protected void renderProjects(@Nullable final List<ProjectDTO> projects) throws ProjectNotFoundException {
        if (projects == null) throw new ProjectNotFoundException();
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project);
            System.out.println();
            index++;
        }
    }

    protected void renderProjectsFullInfo(@Nullable final List<ProjectDTO> projects) throws ProjectNotFoundException {
        if (projects == null) throw new ProjectNotFoundException();
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ".");
            displayProject(project);
            System.out.println();
            index++;
        }
    }

}
