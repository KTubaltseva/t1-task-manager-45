package ru.t1.ktubaltseva.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.endpoint.*;
import ru.t1.ktubaltseva.tm.api.model.ICommand;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return getServiceLocator().getAuthEndpoint();
    }

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        return getServiceLocator().getSystemEndpoint();
    }

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    protected ILoggerService getLoggerService() {
        return getServiceLocator().getLoggerService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    protected ITokenService getTokenService() {
        return getServiceLocator().getTokenService();
    }

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    @Nullable
    public abstract Role[] getRoles();

    public abstract String getArgument();

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += ": " + description;
        return result;
    }

}
