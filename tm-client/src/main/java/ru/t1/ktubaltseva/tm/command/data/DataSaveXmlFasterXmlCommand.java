package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveXmlFasterXmlRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataSaveXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-xml-faster-xml";

    @NotNull
    private final String DESC = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE XML DATA]");
        @NotNull final DataSaveXmlFasterXmlRequest request = new DataSaveXmlFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataXmlFasterXml(request);
    }

}
