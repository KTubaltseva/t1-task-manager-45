package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class DomainTestData {

    @Nullable
    public final static String ADMIN_LOGIN = "ADMIN";

    @Nullable
    public final static String ADMIN_PASSWORD = "ADMIN";

    @Nullable
    public final static String USER_LOGIN = "TEST";

    @Nullable
    public final static String USER_PASSWORD = "TEST";

    @Nullable
    public final static String USER_EMAIL = "USER_EMAIL";

    @Nullable
    public final static String USER_TOKEN_NULL = null;

    @Nullable
    public final static String USER_TOKEN_NON_EXISTENT = "USER_TOKEN_NON_EXISTENT";

}
